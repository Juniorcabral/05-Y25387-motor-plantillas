import express from 'express';
import path from 'path';
import hbs from 'hbs';
import bodyParser from 'body-parser';
const app = express();
const publicRoutes = require('./routes/public.js');
const adminRoutes = require('./routes/admin.js');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

hbs.registerPartials(path.join(__dirname, 'views', 'partials'));

app.use("/admin", adminRoutes);
app.use('/', publicRoutes);

app.listen(3000, () => {
    console.log("Server is running on port http://localhost:3000/");
});