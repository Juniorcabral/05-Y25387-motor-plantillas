const express = require('express');
const PublicController = require('../controllers/public/public.controller');

const router = express.Router();

router.use (PublicController.indexNavbar);

router.use(PublicController.indexFooter);

router.get("/", PublicController.index);

router.get("/curso", PublicController.indexCurso);

router.get("/word_cloud", PublicController.indexWordCloud);

router.get("/:matricula", PublicController.indexIntegrante, PublicController.indexIntegranteByMatricula);


module.exports = router;