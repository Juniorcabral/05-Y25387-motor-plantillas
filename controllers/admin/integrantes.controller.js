/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/

const db = require("../../db/DBUtils");

const IntegrantesController = {
    index: function (req, res) {
        db.getAllIntegrantes().then((integrantes) => {
            res.render("admin/integrantes", { integrantes });
        }).catch((err) => {
            res.render("admin/integrantes", { error: err });
        });
    },
    create: function (req, res) {
        res.render("admin/integrantes/Crear");
    },
    store: async function (req, res) {
        if (req.body) {
            const integrante = req.body;
            req.session.formData = integrante;
            if (!integrante.nombre || !integrante.apellido || !integrante.matricula || !integrante.descripcion || !integrante.orden) {
                return res.render("admin/integrantes/Crear", {
                    error: "Los campos son obligatorios",
                    formData: req.session.formData
                });
            }
            try {
                await db.createIntegrante(integrante);
                res.redirect("/admin/integrantes/listar?success=Integrante creado con éxito");
            } catch (err) {
                res.render('admin/integrante/Crear', {error: err.message, formData: req.session.formData});
            }
        } else {
            res.render("admin/integrante/Crear", {
                error: "No se recibieron datos del formulario",
                formData: req.session.formData
            });
        }
    },
    show: function (req, res) {
        //Se implementara en la siguiente issue
    },
    update: function (req, res) {
        db.editIntegrante(req.params.matricula, req.body)
            .then(() => {
                req.flash('success', '¡Integrante actualizado con éxito!');
                res.redirect("/admin/integrantes/listar");
            })
            .catch((err) => {
                console.error(err);
                req.flash('error', '¡Error al actualizar el integrante!: '+ err);
                res.redirect("/admin/integrantes/listar");
            });
    },
    edit: function (req, res) {
        db.getIntegranteByMatricula(req.params.matricula)
            .then((integrante) => {
                res.render("admin/integrantes/Editar", { integrante });
            })
            .catch((err) => {
                res.render("admin/integrantes/Editar", { error: err });
            });
    },
    destroy: function (req, res) {
        db.deleteIntegrante(req.params.matricula)
            .then(() => {
                req.flash('success', '¡Integrante eliminado con éxito!');
                res.redirect("/admin/integrantes/listar");
            })
            .catch((err) => {
                console.error(err);
                req.flash('error', '¡Error al eliminar el integrante!: '+ err);
                res.redirect("/admin/integrantes/listar");
            });
    }
}

module.exports = IntegrantesController;
