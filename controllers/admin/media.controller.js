const db = require("../../db/DBUtils");
const fs = require("fs");


const MediaController = {
    index: function (req, res) {
        db.getMedia().then((media) => {
            res.render("admin/media/index", { media });
        }).catch((err) => {
            res.render("admin/media/index", { error: err });
        });
    },
    create: function (req, res) {
        Promise.all([db.getAllIntegrantes(), db.getTipoMedia()])
            .then(([integrantes, tipoMedia]) => {
                res.render("admin/media/Crear", { integrantes, tipoMedia });
            })
            .catch((err) => {
                res.render("admin/media/Crear", { error: err });
            });
    },
    store: async function (req, res) {
        if (req.body) {
            const media = req.body;
            req.session.formData = media;

            // Verificar si se subió un archivo
            let tmp_path, destino;
            if (req.file) {
                tmp_path = req.file.path;
                destino = "./public/assets/" + req.file.originalname;
                fs.rename(tmp_path, destino, (err) => {
                    if (err) {
                        console.log(err);
                        return res.render("admin/media/Crear", { error: "No se pudo guardar la imagen", formData: req.session.formData });
                    }
                });
            }
            // Validación de los campos del formulario
            if (!media.integrante || !media.tipo_media || !media.orden || !media.titulo || !media.descripcion) {
                return res.render("admin/media/Crear", { error: "Los campos son obligatorios", formData: req.session.formData });
            }
            // Crear media sin la imagen si no se subió ninguno
            db.createMedia(media, req.file? '/assets/' + req.file.originalname : null).then(() => {
                res.redirect(`/admin/media/listar?success=Media creada con éxito`);
            }).catch((err) => {
                res.render("admin/media/Crear", { error: err.message, formData: req.session.formData });
            });
        } else {
            res.render("admin/media/Crear", { error: "No se recibieron datos del formulario", formData: req.session.formData });
        }
    },
    show: function (req, res) {
        //Se implementara en la siguiente issue
    },
    update: function (req, res) {
        const media = req.body;
        const mediaID = req.params.mediaID;
        console.log(media);
        console.log(mediaID);
        db.updateMedia(mediaID, media)
            .then(() => {
                req.flash('success', '¡Media actualizada con éxito!');
                res.redirect("/admin/media/listar");
            })
            .catch((err) => {
                console.error(err);
                req.flash('error', '¡Error al actualizar la media!: ' + err);
                res.redirect("/admin/media/listar");
            });
    },
    edit: function (req, res) {
        db.getMediaID(req.params.mediaID)
            .then((media) => {
                res.render("admin/media/Editar", { media });
            })
            .catch((err) => {
                res.render("admin/media/Editar", { error: err });
            });
    },
    destroy: function (req, res) {
        db.deleteTipoMedia(req.params.mediaID)
            .then(() => {
                req.flash('success', '¡Media eliminada con éxito!');
                res.redirect("/admin/media/listar");
            })
            .catch((err) => {
                console.error(err);
                req.flash('error', '¡Error al eliminar la media!: ' + err);
                res.redirect("/admin/media/listar");
            });
    }
}

module.exports = MediaController;