const db = require("../../db/DBUtils");

const TipoMediaController = {
    index: function (req, res) {
        db.getTipoMedia().then((tipoMedia) => {
            res.render("admin/tipo_media", { tipoMedia });
        }).catch((err) => {
            res.render("admin/tipo_media", { error: err });
        });
    },
    create: function (req, res) {
        res.render("admin/tipo_media/Crear");
    },
    store: async function (req, res) {
        if (req.body) {
            const tipoMedia = req.body;
            req.session.formData = tipoMedia;
            // Validación de los campos del formulario
            if (!tipoMedia.nombre ||!tipoMedia.orden) {
                return res.render("admin/tipo_media/Crear", { error: "Los campos son obligatorios", formData: req.session.formData });
            }
            // Creación del tipo media
            db.createTipoMedia(tipoMedia).then(() => {
                res.redirect("/admin/tipo_media/listar?success=Tipo de media creado con éxito");
            }).catch((err) => {
                res.render("admin/tipo_media/Crear", { error: err.message, formData: req.session.formData });
            });
        } else {
            res.render("admin/tipo_media/Crear", { error: "No se recibieron datos del formulario", formData: req.session.formData });
        }
    },
    show: function (req, res) {
        //Se implementara en la siguiente issue
    },
    update: function (req, res) {
        db.editTipoMedia(req.body, req.params.tipoMediaID)
            .then(() => {
                req.flash('success', '¡Tipo de media actualizado con éxito!');
                res.redirect("/admin/tipo_media/listar");
            })
            .catch((err) => {
                console.error(err);
                req.flash('error', '¡Error al actualizar el tipo de media!: '+ err);
                res.redirect("/admin/tipo_media/listar");
            });
    },
    edit: function (req, res) {
        db.getTipoMediaByID(req.params.tipoMediaID)
            .then((tipoMedia) => {
                res.render("admin/tipo_media/Editar", { tipoMedia });
            })
            .catch((err) => {
                res.render("admin/tipo_media/Listar", { error: err });
            });
    },
    destroy: function (req, res) {
        db.deleteMedia(req.params.tipoMediaID)
            .then(() => {
                req.flash('success', '¡Tipo de media eliminado con éxito!');
                res.redirect("/admin/tipo_media/listar");
            })
            .catch((err) => {
                console.error(err);
                req.flash('error', '¡Error al eliminar el tipo de media!: '+ err);
                res.redirect("/admin/tipo_media/listar");
            });
    }
}

module.exports = TipoMediaController;